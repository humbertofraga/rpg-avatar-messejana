FROM ruby:2.7

ENV JEKYL_ENV production
ENV LC_ALL c.UTF-8

WORKDIR /usr/src/app

COPY src/Gemfile src/Gemfile.lock ./

RUN bundle install

COPY src ./

RUN bundle exec jekyll build -d public

EXPOSE 4000

CMD [ "bundle", "exec", "jekyll", "serve", "-H", "0.0.0.0", "-P", "4000" ]
