![Build Status](https://gitlab.com/humbertofraga/rpg-avatar-messejana/badges/master/pipeline.svg)
![Jekyll Version](https://img.shields.io/gem/v/jekyll.svg)

---

# RPG Avatar Messejana

Um site simples listando as fichas de personagens e outras informações da Campanha.
